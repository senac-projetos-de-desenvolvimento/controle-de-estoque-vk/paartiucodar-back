<h1>Bem vindo ao Back-End do projeto Conig Plataforma de Aprendizagem</h1>

Para acessar o nosso site basta clicar [aqui](https:\\app.conig.info) ou digitar no seu navegador <strong>app.conig.info</strong>!
Alguns usuários de teste:

Email: alunoteste@gmail.com <br>
Senha: conig123

Email: alunoteste2@gmail.com <br>
Senha: conig123

**Os usuários de testes vão ser resetados periodicamente**


Caso queira rodar o projeto **diretamente na sua máquina**, siga o seguinte passo a passo:

Requisitos para rodar o projeto:

**Windows:**
- Docker desktop
- Container no docker com o [Redis](https://redis.io/)
- [Node](https://nodejs.org/en/) instalado no seu computador
- [Adonis 4 (legado)](https://legacy.adonisjs.com/)<br>

**Unix:**
- [Redis](https://redis.io/)
- [Adonis 4 (legado)](https://legacy.adonisjs.com/)
- [Node](https://nodejs.org/en/)


<h1>Como instalar e rodar o projeto:</h1>

<h2>Instalando o Docker e o Redis no Windows</h2>

- Baixe o [Docker Desktop](https://www.docker.com/products/docker-desktop) e instale o mesmo.
- Após o término da instalação do **Docker Desktop**, abra o **PowerShell** em modo administrador e roda os seguintes comandos separadamente e em ordem:
    - `Set-ExecutionPolicy unrestricted`
    - `docker pull redis`
    - `docker network create --driver bridge network`
    - `docker run --name redis --network=network -p 6379:6379 -d redis`
- Feito isso, seu redis deve estar rodando na porta **6379**, agora vamos para o projeto.

<h2>Instalando o Redis no Unix</h2>

- Para instalar o Redis no Unix é muito simples, basta abrir o terminal e rodar os seguintes comandos:
    - `wget http://download.redis.io/releases/redis-3.2.5.tar.gz`
    - `tar xzf redis-3.2.5.tar.gz`
    - `cd redis-3.2.5`
    - `make`
- Pronto, Redis instalado com sucesso agora só precisamos inicia-lo, e para isso utilizaremos o seguinte comando (Lembre-se, esteja dentro do diretório em que o Redis foi instalado):
    - `src/redis-server`

<h2>Configurando e rodando o projeto</h2>

- Clone este repósitorio para a sua máquina.
- Ao abrir o seu projeto no **Visual Studio Code**, abra o terminal do mesmo e rode os seguintes comandos:
    - `npm i -g @adonisjs/cli`
    - `npm install`
- Após rodar estes comandos, copie e cole o arquivo **.env example** e o renomeie para **.env**.
- Feito a renomeação configure-o da seguinte maneira: (Lembre-se é muito importante já ter um **banco de dados** criado no seu **mysql**)


```
HOST=127.0.0.1 <br>
PORT=3333 <br>
NODE_ENV=development <br>
APP_URL=http://${HOST}:${PORT} <br>
CACHE_VIEWS=false <br>
APP_KEY={AINDA A SER GERADO} <br>
DB_CONNECTION=mysql <br>
DB_HOST=127.0.0.1 <br>
DB_PORT=3306 <br>
DB_USER={SEU USUARIO DO MYSQL} <br>
DB_PASSWORD={SUA SENHA DO MYSQL} <br>
DB_DATABASE={NOME DO SEU BANCO DE DADOS} <br>
SESSION_DRIVER=cookie <br>
HASH_DRIVER=bcrypt <br>
REDIS_HOST=127.0.0.1 <br>
REDIS_PORT=6379 <br>
REDIS_PASSWORD= <br>
REDIS_DB=0 <br>
REDIS_KEY_PREFIX= <br>
BULL_UI_PORT=9379 <br>
MAIL_PORT=587 <br>
MAIL_HOST=smtp.gmail.email <br>
MAIL_USERNAME={SEU EMAIL} <br>
MAIL_PASSWORD={SENHA DO SEU EMAIL} <br>
MAIL_SECURE=false <br>
```
- Agora roda o seguinte comando para gerar usa APP_KEY:
    - `adonis key:generate`
- Com seu .env configurado, rode o seguinte comando para rodar as migrações:
    - `adonis migration:run `
- Com as migrações feitas, agora tudo o que resta é iniciar o projeto adonis (Lembre-se de estar com o redis rodando):
    - `adonis serve --dev`

Parabéns você rodou com sucesso o projeto na sua máquina :smile: :rocket:.

<h1>Onde eu encontro o WebApp?</h1>

Você consegue encontrar o WebApp [clicando aqui!](https://gitlab.com/projeto-de-desenvolvimento/controle-de-estoque-vk/schoolbooks-mobile)




