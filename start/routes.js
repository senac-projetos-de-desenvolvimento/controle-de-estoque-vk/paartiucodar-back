/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.resource('students', 'StudentController')
	.apiOnly()
	.validator(new Map([[['students.store'], ['StudentValidator']]]))
Route.resource('modules', 'ModuleController')
	.apiOnly()
	.middleware(new Map([[['modules.show'], ['auth:jwt']]]))
Route.resource('tutorials', 'TutorialController').apiOnly().middleware(['auth:jwt'])
Route.resource('exercises', 'ExerciseController').apiOnly().middleware(['auth:jwt'])
Route.resource('tutorialFeedbacks', 'TutorialFeedbackController').apiOnly().middleware(['auth:jwt'])
Route.resource('studentResponse', 'StudentQuestionResponseController')
	.apiOnly()
	.middleware(['auth:jwt'])
Route.resource('studentTutorial', 'StudentTutorialController').apiOnly().middleware(['auth:jwt'])
Route.resource('studentExercise', 'StudentExerciseController').apiOnly().middleware(['auth:jwt'])
Route.resource('forgot', 'ForgotPasswordController')
	.apiOnly()
	.validator(new Map([[['forgot.store'], ['ForgotPasswordValidator']]]))
Route.resource('reset', 'ResetPasswordController')
Route.resource('rankings', 'RankingController').apiOnly()
Route.resource('profile', 'ProfileController')
	.apiOnly()
	.middleware(new Map([[['profile.index', 'profile.store', 'profile.show'], ['auth:jwt']]]))
Route.resource('login', 'AuthController')
	.apiOnly()
	.validator(new Map([[['login.store'], ['LoginValidator']]]))
