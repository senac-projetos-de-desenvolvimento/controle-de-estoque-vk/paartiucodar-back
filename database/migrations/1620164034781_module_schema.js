'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ModuleSchema extends Schema {
	up() {
		this.create('modules', (table) => {
			table.increments()
			//if it's a language e.g. 'LPYTHON', 'LJSCRIPT', 'LPHP___', 'LCSHARP_'
			//if it's a framework e.g. 'FREACT_', 'FADONIS', 'FVUE___'
			//if it's a technology e.g. 'TDOCKER', 'TGIT___', 'TMONGODB', 'TGRAPHQ'
			//if it's a concept e.g. 'CALGORITHM', 'CCOMPUTING'
			table.string('sys_id').notNullable().unique()
			//Fundamentos de Javascript
			table.string('name', 45).notNullable()
			table.enu('language', ['JS', 'PY', 'C#', 'NONE']).notNullable()
			//for future use
			table.string('module_image_link', 255)
			//Bem vindo ao módulo de Javascript. Neste módulo você irá aprender o básico para ser
			//um programador Javascript, bem como instalar o VS Code e o Node.js
			table.text('description').notNullable()
			//O nível que o estudante precisa ter para acessar o módulo
			table.integer('level').notNullable()
			//For future use
			//table.date('included_in')
			//table.date('last_time_updated')
			//table.integer('xp_gained_concluded')
			//table.integer('coins_gained_concluded')
			table.timestamps()
		})
	}

	down() {
		this.drop('modules')
	}
}

module.exports = ModuleSchema
