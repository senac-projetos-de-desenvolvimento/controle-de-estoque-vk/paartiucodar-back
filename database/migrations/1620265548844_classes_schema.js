'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClassesSchema extends Schema {
	up() {
		this.create('classes', (table) => {
			table.increments()
			//For future use
			table.string('sys_id').notNullable()
			//Iniciando com variáveis
			table.string('title').notNullable()
			//Dentro do módulo em qual posição ele está
			table.integer('number').notNullable()
			table
				.integer('module_id')
				.unsigned()
				.references('id')
				.inTable('modules')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('classes')
	}
}

module.exports = ClassesSchema
