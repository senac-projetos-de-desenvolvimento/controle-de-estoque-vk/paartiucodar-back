'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TutorialFeedbacksSchema extends Schema {
	up() {
		this.create('tutorial_feedbacks', (table) => {
			table.increments()
			table.enu('feedback_type', ['GOSTEI', 'ERRO', 'OUTROS']).notNullable()
			table.string('feedback_text').notNullable()
			table
				.integer('tutorial_id')
				.unsigned()
				.references('id')
				.inTable('tutorials')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table
				.integer('student_id')
				.unsigned()
				.references('id')
				.inTable('students')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('tutorial_feedbacks')
	}
}

module.exports = TutorialFeedbacksSchema
