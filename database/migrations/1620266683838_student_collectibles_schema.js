"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class StudentCollectiblesSchema extends Schema {
  up() {
    this.create("student_collectibles", (table) => {
      table.increments();
      table
        .integer("student_id")
        .unsigned()
        .references("id")
        .inTable("students")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table
        .integer("collectible_id")
        .unsigned()
        .references("id")
        .inTable("collectibles")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table.dateTime("acquired_on").notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("student_collectibles");
  }
}

module.exports = StudentCollectiblesSchema;
