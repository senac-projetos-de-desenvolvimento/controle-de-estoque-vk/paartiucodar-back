"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class StudentLevelsSchema extends Schema {
  up() {
    this.create("student_levels", (table) => {
      table.increments();
      table
        .integer("level_id")
        .unsigned()
        .references("id")
        .inTable("levels")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table
        .integer("student_id")
        .unsigned()
        .references("id")
        .inTable("students")
        .onDelete("CASCADE")
        .onUpdate("CASCADE")
        .notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("student_levels");
  }
}

module.exports = StudentLevelsSchema;
