'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CollectiblesSchema extends Schema {
	up() {
		this.create('collectibles', (table) => {
			table.increments()
			table.string('description').notNullable()
			table.string('svg').notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('collectibles')
	}
}

module.exports = CollectiblesSchema
