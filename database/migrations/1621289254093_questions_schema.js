'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionsSchema extends Schema {
	up() {
		this.create('questions', (table) => {
			table.increments()
			//enunciado
			table.string('statement').notNullable()
			//pergunta
			table.text('question').notNullable()
			//resposta correta
			table.string('response').notNullable()
			//o que ele envia para o aluno de volta sobre a questão
			table.text('response_feedback')
			table.integer('position').notNullable() //dentra do exercicio em que posição ela está
			table
				.integer('exercise_id')
				.unsigned()
				.notNullable()
				.references('id')
				.inTable('exercises')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('questions')
	}
}

module.exports = QuestionsSchema
