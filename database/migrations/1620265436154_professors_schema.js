"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ProfessorsSchema extends Schema {
  up() {
    this.create("professors", (table) => {
      table.increments();
      table.string("name").notNullable();
      table.string("email").notNullable();
      table.string("password").notNullable();
      table.string("institutional_email").nullable();
      table.string("personal_website").nullable();
      table
        .integer("college_id")
        .unsigned()
        .references("id")
        .inTable("colleges")
        .notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("professors");
  }
}

module.exports = ProfessorsSchema;
