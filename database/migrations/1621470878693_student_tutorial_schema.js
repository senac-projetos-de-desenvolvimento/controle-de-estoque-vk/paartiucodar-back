'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentTutorialSchema extends Schema {
	up() {
		this.create('student_tutorials', (table) => {
			table.increments()
			table
				.integer('tutorial_id')
				.unsigned()
				.references('id')
				.inTable('tutorials')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table
				.integer('student_id')
				.unsigned()
				.references('id')
				.inTable('students')
				.onDelete('CASCADE')
				.onUpdate('CASCADE')
				.notNullable()
			table.timestamps()
		})
	}

	down() {
		this.drop('student_tutorials')
	}
}

module.exports = StudentTutorialSchema
