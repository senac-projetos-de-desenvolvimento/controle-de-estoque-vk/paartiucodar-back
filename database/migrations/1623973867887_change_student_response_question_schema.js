'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeStudentResponseQuestionSchema extends Schema {
	up() {
		this.table('student_response_questions', (table) => {
			table.string('response').notNullable().alter()
		})
	}

	down() {
		this.table('student_response_questions', (table) => {
			table.boolean('response')
		})
	}
}

module.exports = ChangeStudentResponseQuestionSchema
