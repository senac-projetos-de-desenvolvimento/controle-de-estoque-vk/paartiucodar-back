'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StudentResponseQuestion extends Model {
	question() {
		return belongsTo('App/models/Question')
	}

	student() {
		return belongsTo('App/models/Student')
	}
}

module.exports = StudentResponseQuestion
