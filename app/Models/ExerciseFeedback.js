"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class ExerciseFeedback extends Model {
  exercise() {
    return this.belongsTo("App/Models/Exercise");
  }
  student() {
    return this.belongsTo("App/Models/Student");
  }
}

module.exports = ExerciseFeedback;
