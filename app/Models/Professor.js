"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Professor extends Model {
  college() {
    return this.hasOne("App/Models/College");
  }
}

module.exports = Professor;
