'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Question extends Model {
	static boot() {
		super.boot()

		this.addTrait('@provider:Lucid/Slugify', {
			fields: {
				sys_id: 'title',
			},
			strategy: 'dbIncrement',
		})
	}

	exercise() {
		return belongsTo('App/Models/Exercise')
	}

	studentResponse() {
		return this.hasMany('App/Models/StudentResponseQuestion')
	}
}

module.exports = Question
