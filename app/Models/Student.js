'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')
class Student extends Model {
	static boot() {
		super.boot()

		/**
		 * A hook to hash the user password before saving
		 * it to the database.
		 */

		this.addHook('beforeSave', async (userInstance) => {
			if (userInstance.dirty.password) {
				userInstance.password = await Hash.make(userInstance.password)
			}
		})
	}

	modules() {
		return this.hasMany('App/Models/Modules')
	}
	exerciseFeedbacks() {
		return this.hasMany('App/Modes/ExerciseFeedback')
	}
	tutorialFeedbacks() {
		return this.hasMany('App/Models/TutorialFeedback')
	}
	studentLevel() {
		return this.hasOne('App/Models/StudentLevel')
	}
	studentProfile() {
		return this.hasOne('App/Models/StudentProfile')
	}
	studentCollectibles() {
		return this.hasMany('App/Models/StudentCollectible')
	}
	studentResponse() {
		return this.hasMany('App/Models/StudentResponseQuestion')
	}

	studentTutorial() {
		return this.hasMany('App/Models/StudentTutorial')
	}

	studentExercise() {
		return this.hasMany('App/Models/StudentExercise')
	}

	studentXp() {
		return this.hasMany('App/Models/StudentXp')
	}
}

module.exports = Student
