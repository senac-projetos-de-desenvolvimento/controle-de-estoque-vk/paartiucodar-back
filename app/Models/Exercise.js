/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Exercise extends Model {
	static boot() {
		super.boot()

		this.addTrait('@provider:Lucid/Slugify', {
			fields: {
				sys_id: 'title',
			},
			strategy: 'dbIncrement',
		})
	}

	class() {
		return this.belongsTo('App/Models/Class')
	}

	exerciseFeedbacks() {
		return this.hasMany('App/Models/ExerciseFeedback')
	}

	questions() {
		return this.hasMany('App/Models/Question')
	}

	studentExercise() {
		return this.hasMany('App/Models/StudentExercise')
	}
}

module.exports = Exercise
