const Antl = use('Antl')

class StudentValidator {
	get validateAll() {
		return true
	}

	get rules() {
		return {
			email: 'unique:students,email|email|required',
			username: 'unique:students,username|required',
			password: 'min:6|max:10|required|regex:.*[0-9].*',
		}
	}

	get sanitizationRules() {
		return {
			email: 'trim',
		}
	}

	get messages() {
		return Antl.forLocale('pt').list('validation')
	}
}

module.exports = StudentValidator
