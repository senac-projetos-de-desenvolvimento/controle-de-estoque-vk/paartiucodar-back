/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Module = use('App/Models/Module')
const Tutorial = use('App/Models/Tutorial')
/**
 * Resourceful controller for interacting with modules
 */

class ModuleController {
	/**
	 * Show a list of all modules.
	 * GET modules
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {
		const modules = await Module.query().withCount('classes').fetch()

		return response.status(200).json(modules.toJSON())
	}

	/**
	 * Create/save a new module.
	 * POST modules
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const data = request.only([
			'name',
			'language',
			'module_image-link',
			'description',
			'level',
			'included_in',
			'last_time_updated',
			'xp_gained_concluded',
			'coins_gained_concluded',
		])

		const module = await Module.create(data)

		return response.status(200).json(module.toJSON())
	}

	/**
	 * Display a single module.
	 * GET modules/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view, auth }) {
		const user = await auth.getUser()
		const module = await Module.query()
			.with('classes', (builder) =>
				builder
					.select('id', 'sys_id', 'title', 'module_id')
					.with('tutorials', (tutorialBuilder) =>
						tutorialBuilder
							.select('id', 'class_id', 'title', 'position')
							.with('studentTutorial', (builder) => builder.where('student_id', user.id))
							.orderBy('position', 'asc')
					)
					.with('exercises', (exerciseBuilder) =>
						exerciseBuilder
							.select('id', 'sys_id', 'title', 'class_id')
							.with('studentExercise', (builder) => builder.where('student_id', user.id))
					)
					.orderBy('number')
			)
			.where('id', params.id)
			.first()

		const tutorial = await Tutorial.query()
			.where('class_id', module.toJSON().classes[0].id)
			.where('position', '1')
			.first()

		return response.status(200).json({ module: module.toJSON(), tutorial: tutorial.toJSON() })
	}

	/**
	 * Update module details.
	 * PUT or PATCH modules/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {}

	/**
	 * Delete a module with id.
	 * DELETE modules/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
		const module = await Module.findOrFail(params.id)

		await module.delete()
	}
}

module.exports = ModuleController
