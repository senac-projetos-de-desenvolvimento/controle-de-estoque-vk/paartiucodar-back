const Student = use('App/Models/Student')

class ResetPasswordController {
	async store({ request, response }) {
		const { password, token } = request.only(['password', 'token'])

		const student = await Student.query().where('confirmation_token', token).first()

		if (student.confirmation_token === token) {
			student.confirmation_token = null
			student.password = password
			await student.save()
		}

		return response.status(200).json(student)
	}
}

module.exports = ResetPasswordController
