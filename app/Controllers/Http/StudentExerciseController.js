const Student = use('App/Models/Student')
const StudentXp = use('App/Models/StudentXp')
const StudentExercise = use('App/Models/StudentExercise')
const Bull = use('Rocketseat/Bull')
const StudentCollectibles = use('App/Jobs/Collectibles/StudentCollectibles')

class StudentExerciseController {
	async store({ request, auth, response }) {
		const { exercise_id } = request.only(['exercise_id'])
		const user = await auth.getUser()
		const student = await Student.query().where('id', user.id).first()

		const studentExercise = await StudentExercise.create({
			student_id: user.id,
			exercise_id,
		})

		await StudentXp.create({
			student_id: student.id,
			experience: 75,
		})

		student.experience += 50
		student.save()
		Bull.add(StudentCollectibles.key, {
			id: student.id,
			levelup: false,
		})

		return response.status(200).json(studentExercise.toJSON())
	}
}

module.exports = StudentExerciseController
