/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Tutorial = use('App/Models/Tutorial')
const Exercise = use('App/Models/Exercise')
const StudentTutorial = use('App/Models/StudentTutorial')
/**
 * Resourceful controller for interacting with tutorials
 */

class TutorialController {
	/**
	 * Show a list of all tutorials.
	 * GET tutorials
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view, auth }) {
		const tutorials = await Tutorial.query().withCount('tutorialFeedbacks').fetch()

		return response.status(200).json(tutorials.toJSON())
	}

	/**
	 * Create/save a new tutorial.
	 * POST tutorials
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const data = request.only(['title', 'position', 'description', 'topic_covered', 'content'])

		const tutorial = await Tutorial.create(data)

		return response.status(200).json(tutorial.toJSON())
	}

	/**
	 * Display a single tutorial.
	 * GET tutorials/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, response, view, auth }) {
		const { id } = await auth.getUser()
		const tutorial = await Tutorial.query()
			.with('class', (builder) => builder.select('id', 'module_id'))
			.where('id', params.id)
			.first()
		const student_tutorials = await StudentTutorial.query()
			.where('student_id', id)
			.where('tutorial_id', params.id)
			.first()

		tutorial.is_completed = !!student_tutorials
		const next_tutorial = await Tutorial.query()
			.with('class', (builder) =>
				builder.select('id', 'module_id').where('module_id', tutorial.toJSON().class.module_id)
			)
			.where('class_id', tutorial.toJSON().class_id)
			.where('position', tutorial.toJSON().position + 1)
			.first()

		if (!next_tutorial) {
			const next_tutorial = await Exercise.query()
				.with('class', (builder) =>
					builder.select('id', 'module_id').where('module_id', tutorial.toJSON().class.module_id)
				)
				.where('class_id', tutorial.toJSON().class_id)
				.first()

			tutorial.next_tutorial = next_tutorial ? `/exercise/${next_tutorial.toJSON().id}` : 'last'
		} else {
			tutorial.next_tutorial = `/tutorial/${next_tutorial.toJSON().id}`
		}

		return response.status(200).json(tutorial.toJSON())
	}

	/**
	 * Update tutorial details.
	 * PUT or PATCH tutorials/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {}

	/**
	 * Delete a tutorial with id.
	 * DELETE tutorials/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {}
}

module.exports = TutorialController
