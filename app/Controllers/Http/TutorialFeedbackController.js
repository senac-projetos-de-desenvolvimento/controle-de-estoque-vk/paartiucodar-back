'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const TutorialFeedback = use('App/Models/TutorialFeedback')
/**
 * Resourceful controller for interacting with tutorialfeedbacks
 */
class TutorialFeedbackController {
	/**
	 * Show a list of all tutorialfeedbacks.
	 * GET tutorialfeedbacks
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {}

	/**
	 * Render a form to be used for creating a new tutorialfeedback.
	 * GET tutorialfeedbacks/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {}

	/**
	 * Create/save a new tutorialfeedback.
	 * POST tutorialfeedbacks
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const { feedback_type, feedback_text, student_id, tutorial_id } = request.only([
			'feedback_type',
			'feedback_text',
			'student_id',
			'tutorial_id',
		])

		const feedback = await TutorialFeedback.create({
			feedback_type,
			feedback_text,
			tutorial_id,
			student_id,
		})

		await feedback.loadMany({
			student: null,
			tutorial: null,
		})
		return response.status(200).json(feedback.toJSON())
	}

	/**
	 * Display a single tutorialfeedback.
	 * GET tutorialfeedbacks/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {}

	/**
	 * Render a form to update an existing tutorialfeedback.
	 * GET tutorialfeedbacks/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {}

	/**
	 * Update tutorialfeedback details.
	 * PUT or PATCH tutorialfeedbacks/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {}

	/**
	 * Delete a tutorialfeedback with id.
	 * DELETE tutorialfeedbacks/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {}
}

module.exports = TutorialFeedbackController
