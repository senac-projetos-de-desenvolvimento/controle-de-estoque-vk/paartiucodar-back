const Database = use('Database')
const dayjs = use('dayjs')
const StudentXp = use('App/Models/StudentXp')

class RankingController {
	async index({ response }) {
		const general_ranking = await StudentXp.query()
			.select(Database.raw('sum(experience) as total'), 'student_id')
			.with('student', (builder) => builder.select('id', 'username'))
			.groupBy('student_id')
			.orderBy('total', 'desc')
			.limit(10)
			.fetch()

		const month_ranking = await StudentXp.query()
			.select(Database.raw('sum(experience) as total'), 'student_id')
			.with('student', (builder) => builder.select('id', 'username'))
			.where('created_at', '>=', dayjs().startOf('MONTH').format('YYYY-MM-DD'))
			.groupBy('student_id')
			.orderBy('total', 'desc')
			.limit(10)
			.fetch()

		const weekly_ranking = await StudentXp.query()
			.select(Database.raw('sum(experience) as total'), 'student_id')
			.with('student', (builder) => builder.select('id', 'username'))
			.where('created_at', '>=', dayjs().startOf('WEEK').format('YYYY-MM-DD'))
			.groupBy('student_id')
			.orderBy('total', 'desc')
			.limit(10)
			.fetch()

		return response.status(200).json({
			general_ranking,
			month_ranking,
			weekly_ranking,
		})
	}
}

module.exports = RankingController
